import datetime as dt
import matplotlib.pyplot as plt
from matplotlib import style 
import pandas as pd
import pandas_datareader.data as web

style.use('ggplot')

start = dt.datetime(2019,1,1)
end = dt.datetime(2021,11,8)

nameOfStock = 'tsla.csv'

df = web.DataReader('TSLA','yahoo',start,end)
df.to_csv(nameOfStock)
print(df.head)

fromCVS = pd.read_csv(nameOfStock,parse_dates=True,index_col=0)



df['Adj Close'].plot()
df.plot()

plt.show()